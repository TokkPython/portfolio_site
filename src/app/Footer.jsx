import React from "react";
// Footer Component
const Footer = () => {
    return (
        <footer className="p-8">
            <div className="pb-[10em]"></div>

            <h2 className="text-slate-500">
            <p>This website is proudly made using <a href="https://nextjs.org/">NextJS</a> & <a href="https://tailwindcss.com/">Tailwind</a>. Pure CSS sucks.</p>
            © 2023 Thanapat Nartthanarung ·{" "}
            <a href="https://creativecommons.org/licenses/by-sa/4.0/">
                Licensed under CC BY-SA-4.0
            </a>
            </h2>
        </footer>
    );
};
 
export default Footer;