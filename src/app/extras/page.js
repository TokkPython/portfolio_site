'use client'
import { useState, useEffect, useRef } from "react";
import "../App.css";
import Cover from "../Cover.jsx";
import Footer from "../Footer.jsx";

function App() {
    useEffect(() => {
    }, []);

    return (
        <>
            <Cover name="extras"/>
            <div id="body" className="">
                <h1 className="pt-8 pb-2 font-bold text-[2.5em] sm:text-[3em]">
                    Extra Stuff
                </h1>
                <p className="text-sm sm:text-xl text-slate-500 italic">
                    For those cheeky and greedy <br/>easter egg hunters out there.
                </p>
                
                <div className="bg-amber-700 mt-4 mx-2 sm:mx-[18em] py-[1em] rounded-xl">
                    <h2 id="movies" className="font-bold text-[2em]">
                        • Movies •
                    </h2>
                    <p>My Favorite Movies 3x3</p>
                    <div className="grid grid-cols-3 gap-2 sm:gap-0 my-6 px-2 sm:px-0 items-end place-items-center">
                        <div className="flex flex-col items-center gap-3">
                            <p className="">Dead Poets' Society</p>
                            <img src="/assets/movies/deadpoets.jpg"          className="object-cover rounded h-[12em] w-[10em]"/>
                        </div>
                        <div className="flex flex-col items-center gap-2 mt-2">
                            <p className="">The Lone Ranger</p>
                            <img src="/assets/movies/loneranger.jpeg"        className="object-cover rounded h-[12em] w-[10em]"/>
                        </div>
                        <div className="flex flex-col items-center gap-2 mt-2">
                            <p className="">Kingsman</p>
                            <img src="/assets/movies/kingsman.jpg"           className="object-cover rounded h-[12em] w-[10em]"/>

                        </div>
                        <div className="flex flex-col items-center gap-2 mt-2">
                            <p className="">Mission Impossible</p>
                            <img src="/assets/movies/missionimpossible.jpg"  className="object-cover rounded h-[12em] w-[10em]"/>

                        </div>
                        <div className="flex flex-col items-center gap-2 mt-2">
                            <p className="">Now You See Me</p>
                            <img src="/assets/movies/nowyouseeme.jpg"        className="object-cover rounded h-[12em] w-[10em]"/>
                        </div>
                        
                        <div className="flex flex-col items-center gap-2 mt-2">
                            <p className="">Your Name</p>
                            <img src="/assets/movies/yourname.png"           className="object-cover rounded h-[12em] w-[10em]"/>

                        </div>
                        <div className="flex flex-col items-center gap-2 mt-2">
                            <p className="">Haruhi Suzumiya</p>
                            <img src="/assets/movies/haruhi.jpg"             className="object-cover rounded h-[12em] w-[10em]"/>

                        </div>
                        <div className="flex flex-col items-center gap-2 mt-2">
                            <p className="">Tamako Love Story</p>
                            <img src="/assets/movies/tamakostory.jpg"        className="object-cover rounded h-[12em] w-[10em]"/>

                        </div>
                        <div className="flex flex-col items-center gap-2 mt-2">
                            <p className="">Violet Evergarden: The Movie</p>
                            <img src="/assets/movies/violet.jpg"             className="object-cover rounded h-[12em] w-[10em]"/>
                        </div>
                    </div>
                </div>
                <div className="bg-red-800 mt-6 mx-2 sm:mx-[18em] py-[1em] rounded-xl">
                    <h2 id="movies" className="font-bold text-[2em]">
                        • Anime •
                    </h2>
                    <p>My Favorite Anime 3x3</p>
                    <div className="grid grid-cols-3 gap-2 sm:gap-0 my-6 px-2 sm:px-0 items-end place-items-center">
                        <div  className="flex flex-col items-center gap-2 mt-2">
                            <p>Haruhi Suzumiya</p>
                            <img src="/assets/anime/haruhianime.jpg"          className="object-cover rounded h-[12em] w-[10em]"/>
                        </div>
                        <div  className="flex flex-col items-center gap-2 mt-2">
                            <p>Overlord</p>
                            <img src="/assets/anime/overlord.jpg"        className="object-cover rounded h-[12em] w-[10em]"/>
                        </div>
                        <div  className="flex flex-col items-center gap-2 mt-2">
                            <p>Monogatari Series</p>
                            <img src="/assets/anime/monogatari.jpg"           className="object-cover rounded h-[12em] w-[10em]"/>

                        </div>
                        <div  className="flex flex-col items-center gap-2 mt-2">
                            <p>Gurren Lagann</p>
                            <img src="/assets/anime/gurrenlagann.jpg"  className="object-cover rounded h-[12em] w-[10em]"/>

                        </div>
                        <div  className="flex flex-col items-center gap-2 mt-2">
                            <p>One Punch Man</p>
                            <img src="/assets/anime/onepunch.jpg"        className="object-cover rounded h-[12em] w-[10em]"/>
                        </div>
                        
                        <div  className="flex flex-col items-center gap-2 mt-2">
                            <p>Clannad</p>
                            <img src="/assets/anime/clannad.jpg"           className="object-cover rounded h-[12em] w-[10em]"/>

                        </div>
                        <div  className="flex flex-col items-center gap-2 mt-2">
                            <p>Eighty Six</p>
                            <img src="/assets/anime/eightysix.jpg"             className="object-cover rounded h-[12em] w-[10em]"/>

                        </div>
                        <div  className="flex flex-col items-center gap-2 mt-2">
                            <p>Cyberpunk: Edgerunners</p>
                            <img src="/assets/anime/edgerunners.jpg"        className="object-cover rounded h-[12em] w-[10em]"/>

                        </div>
                        <div  className="flex flex-col items-center gap-2 mt-2">
                            <p>Mob Psycho 100</p>
                            <img src="/assets/anime/mobpsycho.jpg"             className="object-cover rounded h-[12em] w-[10em]"/>
                        </div>
                    </div>
                </div>
            </div>
            <Footer/>
        </>
    );
}

export default App;
