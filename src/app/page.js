'use client'
import { useState, useEffect, useRef } from "react";
import "./App.css";
import Cover from "./Cover.jsx";
import Footer from "./Footer.jsx";

function App() {
    useEffect(() => {
    }, []);

    return (

        <>
            <Cover name="home"/>
            <div className="flex flex-col justify-center overflow-hidden">
                <div className="m-5 md:mx-[10em] p-10 shadow-white shadow-lg border-white border-2 rounded-xl hover:border-purple-500 hover:shadow-purple-500 transition ease-in-out">
                    <p className="py-4 pt-8 italic font-[700] sm:text-xl md:text-4xl md:underline decoration-4">
                        "Everything and anything <br/>Me-related. All in one."
                    </p>
                    
                    <p className="md:text-xl text-slate-500 italic">
                        - Me 2023, who had thought too much about what to put here.
                    </p>
                </div>
                <div className="grid content-center my-[7em] sm:my-[0] px-[2.5em] py-[1em] md:text-[6em] font-bold">
                    <img className="z-40 sm:my-[-35px" src="./assets/GameDeveloper-white.svg"/>
                    <img className="z-30 sm:my-[-35px] opacity-50" src="./assets/GameDeveloper-frame.svg"/>
                    <div className="flex gap-x-[2em] z-20 my-20 sm:my-[-100px] md:my-0 place-self-center absolute">
                        <img className="md:h-[2em] xl:h-[3em] 2xl:h-[4em] w-auto -rotate-[55deg]" src="./assets/GameController.png"/>
                        <img className="md:h-[2em] xl:h-[3em] 2xl:h-[4em] w-0 sm:w-auto" src="./assets/headset.png"/>
                    </div>
                    <img className="z-10 sm:my-[-35px]opacity-40 sm:opacity-30" src="./assets/GameDeveloper.svg"/>
                    <img className="z-0 sm:my-[-35px] opacity-40 sm:opacity-10" src="./assets/GameDeveloper.svg"/>
                    <div className="sm:text-[0.1em] xl:text-[0.25em] mt-[12em] sm:my-10 font-light sm:opacity-25 outline outline-white p-3 rounded">
                        <p> Game Designer | Novelist | College Student | Computer Enthusiast | Content Creator</p>
                    </div>
                </div>
                <div className="px-10 sm:px-40 sm:text-xl pb-6">
                    <h1 className="place-content-center mt-10 font-bold text-2xl flex sm:text-[3em] pb-4">
                    <span><div className="text-white outline outline-white p-2 pl-6 sm:p-4 md:p-6 rounded-l-xl inline-block">About</div></span> <span><div className="text-[#2B173D] bg-white p-2 pr-6 sm:p-4 md:p-6 ml-2 sm:-ml-5 md:ml-3 rounded-r-xl inline-block">Me</div></span>
                    </h1>
                    <p className="text-justify py-4 sm:px-40 indent-8">
                        <span className="font-bold sm:text-4xl">Hello, internet!</span> I'm a Thai college student studying in the Creative Technology major and doing a minor in computer science, specializing in the art of game development and design. Alongside my studies, I pour my passion into crafting immersive gaming experiences. While I explore the realms of graphic design and front-end web development, my heart truly beats for bringing virtual worlds to life. Beyond tech, I'm an avid explorer of diverse interests that add depth to my journey.
                    </p>
                    <div className="flex place-content-center">
                        <img className="rounded-xl sm:my-20 h-[20em] sm:w-[70%] object-cover object-right sm:object-top" src="./assets/pixel_bedroom.gif"/>
                    </div>
                    <p className="font-bold text-justify py-4 sm:px-40">
                        Here are some of the things that I do when I'm bored:  
                    </p>
                    <p className="whitespace-nowrap w-0 animate-loop">Watching anime, reading manga, playing visual novels, watching movies, watching youtube videos, listening to podcasts, studying Japanese, making this website and designing websites in general (front-end), playing video games, doodling, doodling but serious (drawing), playing games with my brother, writing novels and short stories, spending time alone, world history, making pixel art, thinking about worldwide problems like global hunger, thinking about politics, thinking about existentialism and pondering about the paradoxes of the cosmos, eating and <a href="https://www.youtube.com/watch?v=dQw4w9WgXcQ">more</a>.</p>
                </div>
                <div id="socials" className="sm:py-4 px-10 sm:px-40 sm:mx-40">
                    <p className="text-justify sm:text-xl indent-8 pt-20 p-6">Here are my socials. Please don't stalk me. I am here to share about who I am. Not looking for dating partners, freelance paparazzis or professional prank callers.</p>
                    <div className="grid grid-cols-2 items-center place-items-center sm:flex sm:flex-row sm:items-end justify-center">
                        <div>
                            <p>Twitter <br/>(aka <a href="https://x.com">x.com</a>)</p>
                            <a href="https://twitter.com/gunisntapotato">
                                <img
                                    className="w-[4em] sm:w-[6em] h-fit rounded-xl m-4 hover:scale-[1.1] hover:border-4 ease-in-out duration-150"
                                    src="/assets/twitter_logo.png"
                                />
                            </a>
                        </div>
                        <div>
                            <p>Instagram</p>
                            <a href="https://www.instagram.com/nottheactualgun/">
                                <img
                                    className="w-[4em] sm:w-[6em] h-fit rounded-xl m-4 hover:scale-[1.1] hover:border-4 ease-in-out duration-150"
                                    src="/assets/instagram_logo.png"
                                />
                            </a>
                        </div>
                        <div>
                            <p>LinkedIn</p>
                            <a href="https://www.linkedin.com/in/thanapat-nartthanarung-627b69229/">
                                <img
                                    className="w-[4em] sm:w-[6em] h-fit rounded-xl m-4 hover:scale-[1.1] hover:border-4 ease-in-out duration-150"
                                    src="/assets/linkedin_logo.png"
                                />
                            </a>
                        </div>
                        <div>
                            <p>Spotify</p>
                            <a href="https://open.spotify.com/user/eere7y0cto09rhoqsg2vxwtmu">
                                <img
                                    className="w-[4em] sm:w-[6em] h-fit rounded-xl m-4 hover:scale-[1.1] hover:border-4 ease-in-out duration-150"
                                    src="/assets/spotify_logo.png"
                                />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <Footer/>
        </>
    );
}

export default App;
