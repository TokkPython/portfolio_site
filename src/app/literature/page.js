'use client'
import { useState, useEffect, useRef } from "react";
import "../App.css";
import Cover from "../Cover.jsx";
import Footer from "../Footer.jsx";

function App() {
    useEffect(() => {
    }, []);

    return (
        <>
            <Cover name="literature"/>
            <div>
                <h1 className="pt-8 pb-2 font-bold text-[2.5em] sm:text-[3em]">
                    Literature
                </h1>
                <p className="text-sm sm:text-xl text-slate-500 italic">
                    "Carpe diem, seize the day.<br/>Gather ye rosebuds while ye may."<br/>- John Keating, Dead Poets' Society
                </p>
                <p className="px-4">//// Under construction. Check the site again soon! :D ////</p>
            </div>
            <Footer/>
        </>
    );
}

export default App;
