'use client'
import { useState, useEffect, useRef } from "react";
import "../App.css";
import Cover from "../Cover.jsx";
import Footer from "../Footer.jsx";

function App() {
    useEffect(() => {
    }, []);

    return (
        <>
            <Cover name="art"/>
            <div>
                <h1 className="pt-8 pb-2 font-bold text-[2.5em] sm:text-[3em]">
                    Art
                </h1>
                <p className="text-sm sm:text-xl text-slate-500 italic">
                    Art is not what you see,<br/>but what you make others see.
                </p>
                <p className="px-4">//// Under construction. Check the site again soon! :D ////</p>
            </div>
            <Footer/>
        </>
    );
}

export default App;
