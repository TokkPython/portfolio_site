import React from "react";
import { useState, useEffect, useRef } from "react";
// import { useEffect } from "react";
 
const Cover = (props) => {
    const subtitle = useRef();

    const Typewriter = ({ text, delay }) => {
        const [currentText, setCurrentText] = useState('');
        const [currentIndex, setCurrentIndex] = useState(0);
        
        useEffect(() => {
          let timeout;
      
          if (currentIndex < text.length) {
            timeout = setTimeout(() => {
              setCurrentText(prevText => prevText + text[currentIndex]);
              setCurrentIndex(prevIndex => prevIndex + 1);
            }, delay*Math.random()+50);
          }
          else{
            subtitle.current.classList.remove("invisible");
            subtitle.current.classList.add("animate-special");
          }
          // else if (infinite) { // ADD THIS CHECK
          //   setCurrentIndex(0);
          //   setCurrentText('');
          // }
      
          return () => clearTimeout(timeout);
        }, [currentIndex, delay, text]);
      
        return <span>{currentText}</span>;
    };

    // console.log();

    useEffect(() => {
        // const url = window.location.pathname;
        // let pg = url.split("/")[1]
        const pg = props.name;
        document.getElementById(pg).classList.add("bg-white");
        document.getElementById(pg).classList.remove("text-white");
        document.getElementById(pg).classList.add("text-slate-500");
    },[]);

    return (
        <div>
            <div className="absolute bg-white pb-[28em] p-10 rounded-full z-0 md:w-screen blur-[64px]">
            </div>
            <div className="z-10 relative bg-[#370068] p-10 text-[#370068] bg-[url('/assets/giphy.gif')] bg-fixed bg-cover bg-blend-darken md:w-screen md:rounded-b-[20em]">
                <a href="https://bio.site/nottherealgun">
                <img
                    className="transition-all transform hover:sm:scale-110 hover:border-none mb-4 inline w-1/4 rounded-full border-solid border-4 border-gray-300 animate-enlarge xl:h-60 xl:w-60"
                    src="/assets/me2.png"
                />
                </a>
                <h1 className="text-3xl md:text-7xl font-bold bg-clip-text text-transparent bg-gradient-to-r from-red-500 to-blue-500 animate-special">
                Thanapat
                </h1>
                <p className="text-xl md:text-5xl font-light text-white">
                <Typewriter text="Nartthanarung" delay={200}/>
                <span
                    className="box-border inline-block w-1 h-10 ml-2 -mb-2 bg-white md:-mb-4 md:h-16 animate-cursor will-change-transform"
                ></span>
                </p>
                <h2 ref={subtitle} className="font-primary invisible text-slate-400 pt-3 md:text-xl">
                Junior Game Developer, Aspiring Artist & Dreaming Novelist
                </h2>
            </div>

            <div id="navbar" className="py-4 sm:flex-col sm:space-y-0 md:flex md:flex-row md:space-x-4 justify-center">
                <a id="home" href="/" className="flex border-solid border-2 border-white p-1 px-4 rounded-xl hover:bg-white text-white">
                    <span className="inline-block mr-1">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M2.25 12l8.954-8.955c.44-.439 1.152-.439 1.591 0L21.75 12M4.5 9.75v10.125c0 .621.504 1.125 1.125 1.125H9.75v-4.875c0-.621.504-1.125 1.125-1.125h2.25c.621 0 1.125.504 1.125 1.125V21h4.125c.621 0 1.125-.504 1.125-1.125V9.75M8.25 21h8.25" />
                        </svg>
                    </span>
                    Home
                </a>
                <a id="resume" href="/resume" className="flex border-solid border-2 border-white p-1 px-4 rounded-xl hover:bg-white text-white">
                    <span className="inline-block mr-1">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M9 12h3.75M9 15h3.75M9 18h3.75m3 .75H18a2.25 2.25 0 002.25-2.25V6.108c0-1.135-.845-2.098-1.976-2.192a48.424 48.424 0 00-1.123-.08m-5.801 0c-.065.21-.1.433-.1.664 0 .414.336.75.75.75h4.5a.75.75 0 00.75-.75 2.25 2.25 0 00-.1-.664m-5.8 0A2.251 2.251 0 0113.5 2.25H15c1.012 0 1.867.668 2.15 1.586m-5.8 0c-.376.023-.75.05-1.124.08C9.095 4.01 8.25 4.973 8.25 6.108V8.25m0 0H4.875c-.621 0-1.125.504-1.125 1.125v11.25c0 .621.504 1.125 1.125 1.125h9.75c.621 0 1.125-.504 1.125-1.125V9.375c0-.621-.504-1.125-1.125-1.125H8.25zM6.75 12h.008v.008H6.75V12zm0 3h.008v.008H6.75V15zm0 3h.008v.008H6.75V18z" />
                        </svg>
                    </span>
                    Resume/CV
                </a>
                <a id="programming" href="/programming" className="flex border-solid border-2 border-white p-1 px-4 rounded-xl hover:bg-white text-white">
                    <span className="inline-block mr-1">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                          <path stroke-linecap="round" stroke-linejoin="round" d="M6.75 7.5l3 2.25-3 2.25m4.5 0h3m-9 8.25h13.5A2.25 2.25 0 0021 18V6a2.25 2.25 0 00-2.25-2.25H5.25A2.25 2.25 0 003 6v12a2.25 2.25 0 002.25 2.25z" />
                        </svg>

                    </span>
                    Programming
                </a>
                <a id="literature" href="/literature" className="flex border-solid border-2 border-white p-1 px-4 rounded-xl hover:bg-white text-white">
                    <span className="inline-block mr-1">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10" />
                        </svg>

                    </span>
                    Literature
                </a>
                <a id="art" href="/art" className="flex border-solid border-2 border-white p-1 px-4 rounded-xl hover:bg-white text-white">
                    <span className="inline-block mr-1">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M9.53 16.122a3 3 0 00-5.78 1.128 2.25 2.25 0 01-2.4 2.245 4.5 4.5 0 008.4-2.245c0-.399-.078-.78-.22-1.128zm0 0a15.998 15.998 0 003.388-1.62m-5.043-.025a15.994 15.994 0 011.622-3.395m3.42 3.42a15.995 15.995 0 004.764-4.648l3.876-5.814a1.151 1.151 0 00-1.597-1.597L14.146 6.32a15.996 15.996 0 00-4.649 4.763m3.42 3.42a6.776 6.776 0 00-3.42-3.42" />
                        </svg>

                    </span>
                    Art
                </a>
                <a id="extras" href="/extras" className="flex border-solid border-2 border-white p-1 px-4 rounded-xl hover:bg-white text-white">
                    <span className="inline-block mr-1">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M11.25 11.25l.041-.02a.75.75 0 011.063.852l-.708 2.836a.75.75 0 001.063.853l.041-.021M21 12a9 9 0 11-18 0 9 9 0 0118 0zm-9-3.75h.008v.008H12V8.25z" />
                        </svg>

                    </span>
                    More..
                </a>
            </div>
        </div>
    );
};
 
export default Cover;