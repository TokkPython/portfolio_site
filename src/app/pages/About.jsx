import { useState, useEffect, useRef } from "react";
import reactLogo from "../assets/react.svg";
import viteLogo from "/vite.svg";
import "../App.css";
import Cover from "../Cover.jsx";
import Footer from "../Footer.jsx";

function App() {
    useEffect(() => {
    }, []);

    return (
        <>
            <Cover/>
            <div id="body" className="text-xl overflow-hidden xl:px-[10em]">
                <h1 className="py-4 font-bold text-[3em]">
                    About <span><div className="text-[#2B173D] bg-white p-4 -ml-2 transform-all -rotate-[10deg] inline-block">Me</div></span>
                </h1>
                <p className="text-justify py-4 px-40 indent-8">
                    Hello, world! My real name is unnecessarily long, but you can call me <span className="highlight">GUN</span> for short. I am a 19-year-old college student studying in the field of Creative Technology, minoring in Computer Science. I specialize in game development and design, but I am interested in graphic design, IoT, and front-end web design too. Other than those nerdy stuff, I enjoy doing other things too...
                </p>
                <p className="font-bold text-justify py-4 px-40">
                    Here are some of the things I'm interested in:  
                </p>
                <p className="whitespace-nowrap w-0 animate-loop">Watching anime, reading manga, playing visual novels, watching movies, watching youtube videos, listening to podcasts, studying Japanese, making this website and designing websites in general (front-end), playing video games, doodling, doodling but serious (drawing), playing games with my brother, writing novels and short stories, spending time alone, world history, making pixel art, thinking about worldwide problems like global hunger, thinking about politics, thinking about existentialism and pondering about the paradoxes of the cosmos, eating and <a href="https://www.youtube.com/watch?v=dQw4w9WgXcQ">more</a>.</p>
                <p className="text-justify py-4 px-40">Here are my socials. Please don't stalk me. I am here to share about who I am. Not looking for dating partners, freelance paparazzis or professional prank callers.</p>

                <div id="socials" className="flex flex-row py-4 px-40 place-items-end">
                    <div className="flex flex-col flex-1 place-items-center">
                        <p>Twitter <br/>(aka <a href="https://x.com">x.com</a>)</p>
                        <a href="https://twitter.com/gunisntapotato">
                            <img
                                className="w-[6em] h-fit rounded-xl m-4 hover:scale-[1.1] hover:border-4 ease-in-out duration-150"
                                src="/assets/twitter_logo.png"
                            />
                        </a>
                    </div>
                    <div className="flex flex-col flex-1 place-items-center">
                        <p>Instagram</p>
                        <a href="https://www.instagram.com/nottheactualgun/">
                            <img
                                className="w-[6em] h-fit rounded-xl m-4 hover:scale-[1.1] hover:border-4 ease-in-out duration-150"
                                src="/assets/instagram_logo.png"
                            />
                        </a>
                    </div>
                    <div className="flex flex-col flex-1 place-items-center">
                        <p>LinkedIn</p>
                        <a href="https://www.linkedin.com/in/thanapat-nartthanarung-627b69229/">
                            <img
                                className="w-[6em] h-fit rounded-xl m-4 hover:scale-[1.1] hover:border-4 ease-in-out duration-150"
                                src="/assets/linkedin_logo.png"
                            />
                        </a>
                    </div>
                    <div className="flex flex-col flex-1 place-items-center">
                        <p>Spotify</p>
                        <a href="https://open.spotify.com/user/eere7y0cto09rhoqsg2vxwtmu">
                            <img
                                className="w-[6em] h-fit rounded-xl m-4 hover:scale-[1.1] hover:border-4 ease-in-out duration-150"
                                src="/assets/spotify_logo.png"
                            />
                        </a>
                    </div>
                </div>
            </div>
            <Footer/>
        </>
    );
}

export default App;
