import { useState, useEffect, useRef } from "react";
import reactLogo from "../assets/react.svg";
import viteLogo from "/vite.svg";
import "../App.css";
import Cover from "../Cover.jsx";
import Footer from "../Footer.jsx";

function App() {
    useEffect(() => {
    }, []);

    return (
        <>
            <Cover/>
            <div id="body" className="text-xl">
                <h1 className="pt-8 pb-2 font-bold text-[3em]">
                    Extra Stuff
                </h1>
                <p className="text-xl text-slate-500 italic">
                    For those cheeky and greedy <br/>easter egg hunters out there.
                </p>
                
                <div>
                    <h2 id="movies" className="pt-8 pb-2 font-bold text-[2em]">
                        • Movies •
                    </h2>
                    <p className="my-4 text-2xl">My Favorite Movies 3x3</p>
                    <div className="grid grid-cols-3 mx-40 my-6 gap-2 place-items-end">
                        <div>
                            <p className="font-italic m-1">Dead Poets' Society (1989)</p>
                            <img src="/assets/movies/deadpoets.jpg"          className="object-cover rounded-xl h-[20em] w-[20em]"/>
                        </div>
                        <div>
                            <p className="font-italic m-1">The Lone Ranger (2013)</p>
                            <img src="/assets/movies/loneranger.jpeg"        className="object-cover rounded-xl h-[20em] w-[20em]"/>
                        </div>
                        <div>
                            <p className="font-italic m-1">Kingsman (2014-2021)</p>
                            <img src="/assets/movies/kingsman.jpg"           className="object-cover rounded-xl h-[20em] w-[20em]"/>

                        </div>
                        <div>
                            <p className="font-italic m-1">Mission Impossible (1996-2023)</p>
                            <img src="/assets/movies/missionimpossible.jpg"  className="object-cover rounded-xl h-[20em] w-[20em]"/>

                        </div>
                        <div>
                            <p className="font-italic m-1">Now You See Me (2013-2016)</p>
                            <img src="/assets/movies/nowyouseeme.jpg"        className="object-cover rounded-xl h-[20em] w-[20em]"/>
                        </div>
                        
                        <div>
                            <p className="font-italic m-1">Your Name (2016)</p>
                            <img src="/assets/movies/yourname.png"           className="object-cover rounded-xl h-[20em] w-[20em]"/>

                        </div>
                        <div>
                            <p className="font-italic m-1">The Disappearance of Haruhi Suzumiya (2010)</p>
                            <img src="/assets/movies/haruhi.jpg"             className="object-cover rounded-xl h-[20em] w-[20em]"/>

                        </div>
                        <div>
                            <p className="font-italic m-1">Tamako Love Story (2014)</p>
                            <img src="/assets/movies/tamakostory.jpg"        className="object-cover rounded-xl h-[20em] w-[20em]"/>

                        </div>
                        <div>
                            <p className="font-italic m-1">Violet Evergarden: The Movie (2020)</p>
                            <img src="/assets/movies/violet.jpg"             className="object-cover rounded-xl h-[20em] w-[20em]"/>
                        </div>
                    </div>
                    <h2 id="series" className="pt-8 pb-2 font-bold text-[2em]">
                        • Series / TV Shows •
                    </h2>
                    <h2 id="games" className="pt-8 pb-2 font-bold text-[2em]">
                        • Games •
                    </h2>
                    <h2 id="anime" className="pt-8 pb-2 font-bold text-[2em]">
                        • Anime •
                    </h2>
                    <p className="my-4 text-2xl">My Favorite Anime 3x3</p>
                    <div className="grid grid-cols-3 mx-40 my-6 gap-2 place-items-end">
                        <div>
                            <p className="font-italic m-1">Haruhi Suzumiya</p>
                            <img src="/assets/anime/haruhianime.jpg"          className="object-cover rounded-xl h-[20em] w-[20em]"/>
                        </div>
                        <div>
                            <p className="font-italic m-1">Overlord</p>
                            <img src="/assets/anime/overlord.jpg"        className="object-cover rounded-xl h-[20em] w-[20em]"/>
                        </div>
                        <div>
                            <p className="font-italic m-1">Monogatari Series</p>
                            <img src="/assets/anime/monogatari.jpg"           className="object-cover rounded-xl h-[20em] w-[20em]"/>

                        </div>
                        <div>
                            <p className="font-italic m-1">Gurren Lagann</p>
                            <img src="/assets/anime/gurrenlagann.jpg"  className="object-cover rounded-xl h-[20em] w-[20em]"/>

                        </div>
                        <div>
                            <p className="font-italic m-1">One Punch Man</p>
                            <img src="/assets/anime/onepunch.jpg"        className="object-cover rounded-xl h-[20em] w-[20em]"/>
                        </div>
                        
                        <div>
                            <p className="font-italic m-1">Clannad</p>
                            <img src="/assets/anime/clannad.jpg"           className="object-cover rounded-xl h-[20em] w-[20em]"/>

                        </div>
                        <div>
                            <p className="font-italic m-1">Eighty Six</p>
                            <img src="/assets/anime/eightysix.jpg"             className="object-cover rounded-xl h-[20em] w-[20em]"/>

                        </div>
                        <div>
                            <p className="font-italic m-1">Cyberpunk: Edgerunners</p>
                            <img src="/assets/anime/edgerunners.jpg"        className="object-cover rounded-xl h-[20em] w-[20em]"/>

                        </div>
                        <div>
                            <p className="font-italic m-1">Mob Psycho 100</p>
                            <img src="/assets/anime/mobpsycho.jpg"             className="object-cover rounded-xl h-[20em] w-[20em]"/>
                        </div>
                    </div>
                    <h2 id="manga" className="pt-8 pb-2 font-bold text-[2em]">
                        • Manga •
                    </h2>
                    <p className="my-4 text-2xl">My Favorite Manga 3x3</p>
                    <div className="grid grid-cols-3 mx-40 my-6 gap-2 place-items-end">
                        <div>
                            <p className="font-italic m-1">Nisekoi</p>
                            <img src="/assets/manga/nisekoi.png"          className="object-cover rounded-xl h-[20em] w-[20em]"/>
                        </div>
                        <div>
                            <p className="font-italic m-1">Berserk</p>
                            <img src="/assets/manga/berserk.jpg"        className="object-cover rounded-xl h-[20em] w-[20em]"/>
                        </div>
                        <div>
                            <p className="font-italic m-1">Chainsaw Man</p>
                            <img src="/assets/manga/csm.jpg"           className="object-cover rounded-xl h-[20em] w-[20em]"/>

                        </div>
                        <div>
                            <p className="font-italic m-1">Spy x Family</p>
                            <img src="/assets/manga/spy_family.png"  className="object-cover rounded-xl h-[20em] w-[20em]"/>

                        </div>
                        <div>
                            <p className="font-italic m-1">The Quintessential Quintuplets</p>
                            <img src="/assets/manga/quint.png"        className="object-cover rounded-xl h-[20em] w-[20em]"/>
                        </div>
                        
                        <div>
                            <p className="font-italic m-1">Komi-san Can't Communicate</p>
                            <img src="/assets/manga/komi.png"           className="object-cover rounded-xl h-[20em] w-[20em]"/>

                        </div>
                        <div>
                            <p className="font-italic m-1">I Want This Love Game to End</p>
                            <img src="/assets/manga/aishteru.jpg"             className="object-cover rounded-xl h-[20em] w-[20em]"/>

                        </div>
                        <div>
                            <p className="font-italic m-1">Pseudo-Harem</p>
                            <img src="/assets/manga/pseudo harem.jpg"        className="object-cover rounded-xl h-[20em] w-[20em]"/>

                        </div>
                        <div>
                            <p className="font-italic m-1">Bizarre Love Triangle</p>
                            <img src="/assets/manga/bizarre.jpg"             className="object-cover rounded-xl h-[20em] w-[20em]"/>
                        </div>
                    </div>
                    <h2 id="vn" className="pt-8 pb-2 font-bold text-[2em]">
                        • Visual Novels •
                    </h2>
                    <h2 id="yt" className="pt-8 pb-2 font-bold text-[2em]">
                        • Youtube •
                    </h2>
                </div>
            </div>
            <Footer/>
        </>
    );
}

export default App;
