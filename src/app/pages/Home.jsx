import { useState, useEffect, useRef } from "react";
import reactLogo from "../assets/react.svg";
import viteLogo from "/vite.svg";
import "../App.css";
import Cover from "../Cover.jsx";
import Footer from "../Footer.jsx";

function App() {
    useEffect(() => {
    }, []);

    return (
        <>
            <Cover/>
            <div className="flex flex-col justify-center text-xl">
                <p className="py-4 pt-8 italic font-[700] text-4xl">
                    "Everything and anything <br/>Me-related. All in one."
                </p>
                <p className="text-xl text-slate-500 italic">
                    - Me 2023, who had thought too much about what to put here.
                </p>
                <p>
                    hello
                </p>
            </div>
            <Footer/>
        </>
    );
}

export default App;
