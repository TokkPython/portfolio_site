import { useState, useEffect, useRef } from "react";
import reactLogo from "../assets/react.svg";
import viteLogo from "/vite.svg";
import "../App.css";
import Cover from "../Cover.jsx";
import Footer from "../Footer.jsx";

function App() {
    useEffect(() => {
    }, []);

    return (
        <>
            <Cover/>
            <div className="text-xl xl:px-[10em]">
                <h1 className="pt-8 pb-2 font-bold text-[3em]">
                    Resume/CV
                </h1>
                <p className="text-xl text-slate-500 italic">
                    With honor and gratitude, I will thereby<br/> show you my greatest successes in life.
                </p>
                <div id="about" className="px-40">
                    <h2 className="pt-8 pb-2 font-bold text-[2em] text-left">
                        About Me
                    </h2>
                    <div className="flex flex-row py-4 space-x-3 place-items-center">
                        <p className="highlight flex grow">Language: Thai, English</p>
                    </div>
                    <p className="text-justify indent-8">
                        Equipped with bright ideas and the ability to look at things from
                        various perspectives, I strive to utilize my knowledge and resources to
                        produce works freely and flexibly. Combining my works of art, blocks
                        of code and the worlds I build, I confidently pave my own paths in
                        earning my successes.
                    </p>
                </div>
                <div id="education" className="px-40">
                    <h2 className="pt-8 pb-2 font-bold text-[2em] text-left">
                        Education
                    </h2>
                    <p className="text-justify py-4">
                        <h2 className="highlight">April 2022 - Present</h2>
                        Mahidol University International College, Bachelor in Arts & Sciences,<br/>
                        Creative Technology Major, <span className="highlight">GPAX 3.67</span> (as of August 2023)
                    </p>
                    <p className="text-justify py-4">
                        <h2 className="highlight">May 2019 - February 2022</h2>
                        Samsenwittayalai School (High School), Sci-Math Route, <span className="highlight">GPAX 3.40</span>
                    </p>
                </div>
                <div id="work" className="px-40">
                    <h2 className="pt-8 pb-2 font-bold text-[2em] text-left">
                        Work Experience
                    </h2>
                    <p className="text-justify py-4 indent-8 flex flex-row">
                        <ul className="marker:text-slate-500 list-disc">
                            <li>
                                MUIC Animation, Games & Comics Club: Project Manager
                            </li>
                            <li>
                                TCC Tech: Outsourced Front-end Flutter Developer
                            </li>
                        </ul>
                        <ul className="text-left">
                            <li>
                                April 2023 - Present
                            </li>
                            <li>
                                April 2023 - Present
                            </li>
                        </ul>
                    </p>
                </div>
                <div id="certs" className="px-40">
                    <h2 className="pt-8 pb-2 font-bold text-[2em] text-left">
                        Certificates
                    </h2>
                    <p className="text-justify py-4 indent-8 flex flex-row">
                        <ul className="marker:text-slate-500 list-disc">
                            <li>
                                NET Design: Computer Arts & Graphics Design Course
                            </li>
                            <li>
                                Samsung Innovation Camp: Ideathon
                            </li>
                            <li>
                                TU-GET (Thammasat University - General English Test) <span className="highlight">760/1000</span>
                            </li>
                            <li>
                                Samsenwittayalai Morality Camp
                            </li>
                            <li>
                                Samsenwittayalai Leadership Development Camp
                            </li>
                            <li>
                                TOEFL iBT <span className="highlight">77/120</span>
                            </li>
                            <li>
                                IELTS <span className="highlight">7.5/9.0</span>
                            </li>
                            <li>
                                Codeavour 2020: AI & Coding Competition Participation
                            </li>
                            <li>
                                SAT (Math) <span className="highlight">660/800</span>
                            </li>
                            <li>
                                SAT (Evidence-based Reading & Writing) <span className="highlight">530/800</span>
                            </li>
                            <li>
                                The Foundations of Fiction (Writing Mastery) Course
                            </li>
                        </ul>

                        <ul>
                            <li>
                                Jan 2017
                            </li>
                            <li>
                                Nov 2019
                            </li>
                            <li>
                                Nov 2019
                            </li>
                            <li>
                                Nov 2019
                            </li>
                            <li>
                                Aug 2020
                            </li>
                            <li>
                                Oct 2020
                            </li>
                            <li>
                                Nov 2020
                            </li>
                            <li>
                                Jan 2021
                            </li>
                            <li>
                                Mar 2021
                            </li>
                            <li>
                                Mar 2021
                            </li>
                            <li>
                                Mar 2022
                            </li>
                        </ul>
                    </p>
                </div>
                <div id="projects" className="px-40">
                    <h2 className="pt-8 pb-2 font-bold text-[2em] text-left">
                        Accomplishments & Projects
                    </h2>
                    <p className="text-justify py-4 indent-8 flex flex-row">
                        <ul className="marker:text-slate-500 list-disc">
                            <li>
                                Line Stickers
                            </li>
                            <li>
                                Itch.io PB Game Jam 4 - Sneaky: <span className="highlight">35th Place</span>
                            </li>
                            <li>
                                Chulabhorn Royal Academy Simulation Center: Frontpage Design
                            </li>
                            <li>
                                Codeacademy: HTML, CSS, Javascript, C# Courses
                            </li>
                            <li>
                                NPO Animators: Manga to Anime Short-Film Contest Submission
                            </li>
                            <li>
                                <a href="https://nottherealgun.medium.com/">Medium.com</a> Blogs, Articles & Fiction
                            </li>
                            <li>
                                <a href="https://leetcode.com/nottherealgun/">Leetcode Exercises</a>
                            </li>
                            <li>
                                Sprafus: Thanapat N. Online Portfolio (this website)
                            </li>
                            <li>
                                <a href="https://nottherealgun.itch.io/skywars">MUIC Skywars Project </a>
                                (Video Game)
                            </li>
                            <li>
                                And <a href="https://nottherealgun.itch.io/" className="underline">
                                various other game projects...
                                </a>
                            </li>
                        </ul>

                        <ul className="text-left">
                            <li>
                                Apr 2019
                            </li>
                            <li>
                                Aug 2020
                            </li>
                            <li>
                                Nov 2021
                            </li>
                            <li>
                                Apr - Aug 2021
                            </li>
                            <li>
                                Sep 2022
                            </li>
                            <li>
                                Ongoing
                            </li>
                            <li>
                                Ongoing
                            </li>
                            <li>
                                Ongoing
                            </li>
                            <li>
                                Ongoing
                            </li>
                        </ul>
                    </p>
                </div>
                <div id="skills" className="px-40">
                    <h2 className="pt-8 pb-2 font-bold text-[2em] text-left">
                        Skills
                    </h2>
                    <div className="flex flex-row">
                        <p className="text-justify py-4 indent-8 flex flex-row flex-1">
                            <ul className="marker:text-slate-500 list-disc">
                                <li>Programming Fundamentals for Python, C, Javascript, GDScript</li>
                                <li>Markup Languages: HTML, CSS</li>
                                <li>Front-end Frameworks: Bootstrap, Tailwind, React</li>
                                <li>Godot Engine (Game Development) - <span className="highlight">4 Years of Experience</span></li>
                                <li>Basics of 2D Game Design & Development</li>
                                <li>Basics of Front-End UI & UX Design (Figma, CSS)</li>
                                <li>Basics of Arduino (IDE, IoT)</li>
                                <li>Basics of Git Version Control</li>
                                <li>Microsoft Office Essentials</li>
                                <li>Basics of Adobe Photoshop & Illustrator</li>
                                <li>Video Editing Basics (Camtasia Studio 9)</li>
                                <li>Basics of Illustrating Human Anatomy & Digital Art (Procreate)</li>
                                <li>Elementary Level Japanese Proficiency</li>
                            </ul>
                        </p>
                        <div className="pl-4 flex flex-col space-y-2 place-content-center">
                            <img className="w-[10em] flex rounded-xl" src="//assets/coding.gif"/>
                            <img className="w-[10em] flex rounded-xl" src="//assets/website.gif"/>
                        </div>
                    </div>
                </div>
                <div id="interests" className="px-40">
                    <h2 className="pt-8 pb-2 font-bold text-[2em] text-left">
                        Interests
                    </h2>
                    <div className="flex flex-row">
                        <p className="text-left py-4 indent-8 flex-1">
                            <ul className="marker:text-slate-500 list-disc">
                                <li>Game Design & Development</li>
                                <li>Front-End Web Design & Development</li>
                                <li>Digital/Concept Art</li>
                                <li>Digital Animation</li>
                                <li>Making Comics, Web Comics & Manga</li>
                                <li>Writing: Novels, Articles & Short Stories</li>
                                <li>Film-making & Cinematography</li>
                                <li>World History</li>
                                <li>Japanese Language & Culture</li>
                            </ul>
                        </p>
                        <div className="pl-4 flex flex-col flex-wrap space-y-2 place-self-end">
                            <img className="w-[10em] flex rounded-xl" src="//assets/game.gif"/>
                            <img className="w-[10em] flex rounded-xl" src="//assets/game2.gif"/>
                        </div>
                        <img className="w-[10em] h-auto object-cover flex ml-2 rounded-xl" src="//assets/animation.gif"/>
                    </div>
                </div>
                <div id="links" className="sm:px-[15em] flex flex-col place-items-center">
                    <h2 className="pt-8 pb-8 font-bold text-[2em] flex">
                        Links
                    </h2>
                    <div className="flex flex-row place-items-end place-content-center flex-wrap">
                        <div className="flex flex-col">
                            <h2>Socials</h2>
                            <a href="/about#socials">
                                <img
                                    className="w-[6em] h-fit rounded-xl m-4 hover:scale-[1.1] hover:border-4 ease-in-out duration-150"
                                    src="/assets/twitter_logo.png"
                                />
                            </a>
                        </div>
                        <div className="flex flex-col">
                            <h2>Itch.io</h2>
                            <a href="https://nottherealgun.itch.io/">
                                <img
                                    className="w-[6em] h-fit rounded-xl m-4 hover:scale-[1.1] hover:border-4 ease-in-out duration-150"
                                    src="/assets/itch.io_logo.svg"
                                />
                            </a>
                        </div>
                        <div className="flex flex-col">
                            <h2>Github</h2>
                            <a href="https://github.com/nottherealgun">
                                <img
                                    className="w-[6em] h-fit rounded-xl m-4 hover:scale-[1.1] hover:border-4 ease-in-out duration-150"
                                    src="/assets/github_logo.png"
                                />
                            </a>
                        </div>
                        <div className="flex flex-col">
                            <h2>Gitlab</h2>
                            <a href="https://gitlab.com/TokkPython">
                                <img
                                    className="w-[6em] h-fit rounded-xl m-4 hover:scale-[1.1] hover:border-4 ease-in-out duration-150"
                                    src="/assets/gitlab_logo.png"
                                />
                            </a>
                        </div>
                        <div className="flex flex-col">
                            <h2>Artist <br/>Linktree</h2>
                            <a href="https://linktr.ee/gunimage">
                                <img
                                    className="w-[6em] h-fit rounded-xl m-4 hover:scale-[1.1] hover:border-4 ease-in-out duration-150"
                                    src="/assets/linktree_logo.png"
                                />
                            </a>
                        </div>
                        <div className="flex flex-col">
                            <h2>Artwork <br/>Showcase</h2>
                            <a href="https://drive.google.com/drive/folders/1O63eHSwQAorZFuj9WxZGITKslSkuH0aM">
                                <img
                                    className="w-[6em] h-[6em] object-cover rounded-xl m-4 hover:scale-[1.1] hover:border-4 ease-in-out duration-150"
                                    src="/assets/gunimage.png"
                                />
                            </a>
                        </div>
                        <div className="flex flex-col">
                            <h2>Leetcode</h2>
                            <a href="https://leetcode.com/nottherealgun/">
                                <img
                                    className="bg-black w-[6em] h-fit rounded-xl m-4 hover:scale-[1.1] hover:border-4 ease-in-out duration-150"
                                    src="/assets/leetcode_logo.png"
                                />
                            </a>
                        </div>
                        <div className="flex flex-col">
                            <h2>Medium</h2>
                            <a href="https://nottherealgun.medium.com/">
                                <img
                                    className="bg-black w-[6em] h-fit rounded-xl m-4 hover:scale-[1.1] hover:border-4 ease-in-out duration-150"
                                    src="/assets/medium_logo.png"
                                />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <Footer/>
        </>
    );
}

export default App;
