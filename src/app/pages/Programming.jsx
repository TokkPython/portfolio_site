import { useState, useEffect, useRef } from "react";
import reactLogo from "../assets/react.svg";
import viteLogo from "/vite.svg";
import "../App.css";
import Cover from "../Cover.jsx";
import Footer from "../Footer.jsx";

function App() {
    let people = (n,color="black") => {
        let elements = [];
        for(let i=0;i<n;i++){
            elements.push(<p className="flex bg-black p-2 rounded-full"></p>);
        }
        for(let i=0;i<(100-n);i++){
            elements.push(<p className="flex bg-gray-300 p-2 rounded-full"></p>);
        }
        return elements;
    };
    const langs = {
        "javascript":63,
        "html":52,
        "python":49,
        "sql":48,
        "typescript":38,
        "bash":32,
        "java":30,
        "c#":27,
        "c++":22,
        "c":19,
    };
    const langNames = {
        "javascript":"JavaScript",
        "html":"HTML/CSS",
        "python":"Python",
        "sql":"SQL",
        "typescript":"TypeScript",
        "bash":"Bash/Shell",
        "java":"Java",
        "c#":"C#",
        "c++":"C++",
        "c":"C",
    };
    const [lang, setLang] = useState("javascript");
    let changeLang = (language) => {
        setLang(language);
        console.log(lang);
    }

    window.addEventListener('load', async () => {
        let video = document.querySelector('video[muted][autoplay]');
        try {
          await video.play();
        } catch (err) {
          video.controls = true;
        }
    });

    useEffect(() => {
    }, []);

    return (
        <>
            <Cover/>
            <div className="text-xl xl:px-[10em]">
                <h1 className="pt-8 pb-2 font-bold text-[3em]">
                    Programming
                </h1>
                <p className="text-xl text-slate-500 italic">
                    Beep boop bop, Beep. > Hello, world.
                </p>
                <div className="py-6 mx-40 flex flex-col space-y-8">
                    <div className="flex flex-col place-content-center">
                        <p className="pt-8 italic font-[600] text-3xl">
                            "Give a man a program, frustrate him<br/>for a day. Teach a man to program,<br/>frustrate him for a lifetime."
                        </p>
                        <p className="text-2xl text-slate-500 italic pt-4">
                            - Muhammad Waseem
                        </p>
                    </div>

                    <p className="text-[1.25em] text-left font-bold">Introduction</p>
                    <p className="text-justify indent-8">
                        Coding is hard but challenging. Challenging in a way that's fun and gives you rewarding results when you've solved it using your own brain. And if you can't, there are always professionals out there who will lend you some knowledge. I hope some day, programming becomes something fun and useful for everyone. A puzzle where anyone can try out. Even for people who think coding is exceedingly difficult. A riddle, perhaps made only for the cleverest.
                    </p>
                    
                    <p className="text-[2em] text-left font-bold">Preferences</p>
                        <ul role="list" className="text-left m-8 marker:text-slate-500 list-disc">
                            <li>
                                <span className="highlight">Languages I am accustomed with:</span> Python, <a href="https://learnxinyminutes.com/docs/gdscript/#:~:text=GDScript%20is%20a%20dynamically%20typed,syntax%20is%20similar%20to%20Python%27s.">GDScript</a>, HTML, CSS
                                
                            </li>
                            <li>
                                <span className="highlight">Languages I know on a basic level:</span> C, C#, JavaScript, Dart
                            </li>
                            <li className="line-through">
                                Languages invented by The Devil: Java, JavaScript, Golang
                            </li>
                            <li>
                                Favorite IDE: Visual Studio Code
                            </li>
                            <li>
                                Favorite Text Editor: Notepad++, Obsidian (if this counts)
                            </li>
                            <li>
                                Favorite Operating System: Windows 10 (I'm sorry, Linux users.)
                            </li>
                            <li>
                                Least Favorite Part of Programming: Math
                            </li>
                        </ul>
                    <div>
                        <p className="mb-4 font-bold">Programming Language Popularity Survey</p>
                        <div className="bg-white rounded-xl p-[2em]">
                            <p className="text-black mb-4 mx-[6em]">Each black dot represents one person out of 100 people who uses 
                                <span className="ml-2">
                                    <select name="language" id="lang" className="cursor-pointer text-white p-1 rounded" onChange={(item) => changeLang(item.target.value)}>
                                        <option value="javascript">Javascript</option>
                                        <option value="html">HTML/CSS</option>
                                        <option value="python">Python</option>
                                        <option value="sql">SQL</option>
                                        <option value="typescript">TypeScript</option>
                                        <option value="bash">Bash/Shell</option>
                                        <option value="java">Java</option>
                                        <option value="c#">C#</option>
                                        <option value="c++">C++</option>
                                        <option value="c">C</option>
                                    </select>.
                                </span><br/><br/>Statistics from <a href="https://stackoverflow.blog/2023/06/13/developer-survey-results-are-in/">The Overflow Developer Survey 2023</a>, 90,000 people all over the world participated. (As of June 2023)</p>
                            <p className="text-black font-bold text-left mb-2">{langNames[lang]} ({langs[lang]}/100)</p>
                            <div className="flex flex-row gap-3 place-content-start flex-wrap">
                                {people(langs[lang])}
                            </div>
                        </div>
                    </div>
                    <p className="text-[2em] text-left font-bold">Game Development</p>
                    <p className="text-justify indent-8">
                        Before high school, I've never found a reason where I could use programming in a fun and interesting way. Coding is just a boring session of typing in rules for computers to use. Back then, it was only a small part of my life as my focus in the moment was intense gaming. It was the one and only thing that could pull me into an unforgettable experience. I played games like Team Fortress 2, Portal 2, Minecraft and more, to distract me in my daily life. Until one day, I was curious enough to go on a journey to research how video games are made. Game engines, sprites, physics systems, pathfinding, boids. These things planted a seed into me. A growing source of curiosity into this new field of knowledge. The Game development industry.
                    </p>
                    <p className="text-[1.25em] text-left font-bold">Unity Engine</p>
                    <p className="text-justify indent-8">
                        As you may know, in the game development industry, the top tier standard game engines include Unity Engine, Unreal Engine, Gamemaker Studio, and even Scratch. Each of them made by their own company. They are proprietary products, made and tuned to suit game development studios and projects. Unity Engine is a good one. Perhaps, the most popular one in this day and age. It uses a very detailed, but efficient components system, accompanied by the well-known software development language, invented by Microsoft, C#. Personally, I think it is quite tedious to use though.
                    </p>
                    <p className="text-[1.25em] text-left font-bold">Godot Engine</p>
                    <p className="text-[1.25em] text-left font-bold highlight">Game Projects</p>
                    <p className="text-justify">Notice: Not everything here is fully released to the public. In fact, most of them are in development and are not yet ready for release. Games that are available to the public right now are: Kollect (demo), Guardians & Corruptors (alpha), Lonund (demo), Nelly's Supply Store (demo), and Skywars (beta). All of these games can be downloaded and played <a href="https://nottherealgun.itch.io/">here</a>. If you want to see behind-the-scenes code, my Gitlab account is <a href="https://gitlab.com/TokkPython">here</a>.</p>
                    <div className="flex flex-row gap-3 place-items-start flex-wrap place-content-center">
                        <div className="bg-white text-black p-2 rounded w-[13em] hover:z-50">
                            <p className="mb-2">Triangul's Quest</p>
                            <img src="/assets/games/triangul.png" className="rounded transition transform ease-in-out duration-300 hover:scale-[1.5]"/>
                        </div>
                        <div className="bg-white text-black p-2 rounded w-[13em] hover:z-50">
                            <p className="mb-2">Galactic Delivery</p>
                            <img src="/assets/games/galactic.png" className="rounded transition transform ease-in-out duration-300 hover:scale-[1.5]"/>
                        </div>
                        <div className="bg-white text-black p-2 rounded w-[13em] hover:z-50">
                            <p className="mb-2">Lonund</p>
                            <img src="/assets/games/lonund.png" className="rounded transition transform ease-in-out duration-300 hover:scale-[1.5]"/>
                        </div>
                        <div className="bg-white text-black p-2 rounded w-[13em] hover:z-50">
                            <p className="mb-2">Guardians & Corruptors</p>
                            <img src="/assets/games/g&c.png" className="rounded transition transform ease-in-out duration-300 hover:scale-[1.5]"/>
                        </div>
                        <div className="bg-white text-black p-2 rounded w-[13em] hover:z-50">
                            <p className="mb-2">Kollect</p>
                            <img src="/assets/games/kollect.png" className="rounded transition transform ease-in-out duration-300 hover:scale-[1.5]"/>
                        </div>
                        <div className="bg-white text-black p-2 rounded w-[13em] hover:z-50">
                            <p className="mb-2">Nelly's Supply Store</p>
                            <img src="/assets/games/nelly.png" className="rounded transition transform ease-in-out duration-300 hover:scale-[1.5]"/>
                        </div>
                        <div className="bg-white text-black p-2 rounded w-[13em] hover:z-50">
                            <p className="mb-2">Skywars</p>
                            <a href="https://nottherealgun.itch.io/skywars"><img src="/assets/games/skywars.png" className="w-screen rounded transition transform ease-in-out duration-300 hover:scale-[1.5]"/></a>
                            
                        </div>
                        <div className="bg-white text-black p-2 rounded w-[13em] hover:z-50">
                                <p className="mb-2">Conqueror of Dinoworld</p>
                                <img src="/assets/games/dinoworld.png" className="rounded transition transform ease-in-out duration-300 hover:scale-[1.5]"/>
                            </div>
                            <div className="bg-white text-black p-2 rounded w-[13em] hover:z-50">
                                <p className="mb-2">Visual Novel Editor</p>
                                <img src="/assets/games/vneditor.png" className="rounded transition transform ease-in-out duration-300 hover:scale-[1.5]"/>
                            </div>
                        
                    </div>

                    <p className="text-[2em] text-right font-bold">Web Design & Development</p>
                    <p className="text-[2em] text-left font-bold">Competitive Coding</p>
                    <p className="text-[2em] text-right font-bold">Other Projects</p>
                    <p className="text-[2em] text-left font-bold">Future Projects</p>
                    <p className="text-[2em] text-right font-bold">Resources</p>
                </div>
            </div>
            <Footer/>
        </>
    );
}

export default App;
