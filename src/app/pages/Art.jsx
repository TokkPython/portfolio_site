import { useState, useEffect, useRef } from "react";
import reactLogo from "../assets/react.svg";
import viteLogo from "/vite.svg";
import "../App.css";
import Cover from "../Cover.jsx";
import Footer from "../Footer.jsx";

function App() {
    useEffect(() => {
    }, []);

    return (
        <>
            <Cover/>
            <div className="text-xl">
                <h1 className="pt-8 pb-2 font-bold text-[3em]">
                    Art
                </h1>
                <p className="text-xl text-slate-500 italic">
                    Art is not what you see,<br/>but what you make others see.
                </p>
            </div>
            <Footer/>
        </>
    );
}

export default App;
