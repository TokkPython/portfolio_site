import { useState, useEffect, useRef } from "react";
import reactLogo from "../assets/react.svg";
import viteLogo from "/vite.svg";
import "../App.css";
import Cover from "../Cover.jsx";
import Footer from "../Footer.jsx";

function App() {
    useEffect(() => {
    }, []);

    return (
        <>
            <Cover/>
            <div className="text-xl">
                <h1 className="pt-8 pb-2 font-bold text-[3em]">
                    Literature
                </h1>
                <p className="text-xl text-slate-500 italic">
                    "Carpe diem, seize the day.<br/>Gather ye rosebuds while ye may."<br/>- John Keating, Dead Poets' Society
                </p>
            </div>
            <Footer/>
        </>
    );
}

export default App;
