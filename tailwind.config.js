/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        'primary':"Cascadia",
        'bold' : "CascadiaBold",
        'light': "CascadiaLight",
        'italic': "CascadiaItalic"
      },
      animation: {
        special: 'special 1.0s ease-out 0s 1',
        special2: 'special2 2.0s ease-out 0s 1',
        cursor: 'cursor 0.5s linear 0s infinite alternate both',
        enlarge: 'enlarge 1.5s ease 0s 1',
        loop: 'loop 60s linear 0s infinite',
      },
      keyframes: {
        special: {
          '0%': { 'letter-spacing': '-0.5em' },
          '100%': { 'letter-spacing': '0em' },
        },
        special2: {
          '0%': { transform:'translateY(-10em)' },
          '90%': { transform:'translateY(10px)' },
          '100%': { transform:'translateY(0px)' },
        },
        cursor:{
          '0%': { opacity:'0%' },
          '100%': { opacity:'100%' },
        },
        enlarge:{
          '0%': { transform:'scale(0)' },
          '75%': { transform:'scale(1.1)' },
          '100%': { transform:'scale(1)' },
        },
        loop:{
          '0%': { 'transform': 'translateX(0em)' },
          '100%': { 'transform': 'translateX(-245em)' },
        },
      },
    },
  },
  plugins: [
    require('@tailwindcss/typography'),
  ],
}

